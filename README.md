#DUXCMS 2.0 BETA版

DUXCMS是一款针对于中小网站与二次开发平台的一款管理系统

2.0版本完全基于THINKPHP重写

最新版基于之前TP版本进行的大部分BUG的修复于优化，后台UI框架采用拼图，更加利于多平台使用于开发者快速开发

更换DUX模板引擎，标签更加接近原生与方便美工使用

环境支持：
php5.3+
mysql5.1

浏览器支持：
ie8+ chrome firefox 
因特性问题暂不支持IE8以下浏览器，后期根据情况做IE8兼容性

----

#说明

默认后台帐号密码为：admin
默认后台地址为：http://域名/index.php?m=admin

----

#交流

QQ群：131331864
 	
网址：http://www.duxcms.com

----

#开发支持

开发团队：Life