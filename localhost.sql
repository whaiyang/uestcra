-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-12-12 09:19:06
-- 服务器版本： 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `uestcra`
--

-- --------------------------------------------------------

--
-- 表的结构 `dux_admin_group`
--

CREATE TABLE IF NOT EXISTS `dux_admin_group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `base_purview` text,
  `menu_purview` text,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`group_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='后台管理组' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `dux_admin_group`
--

INSERT INTO `dux_admin_group` (`group_id`, `name`, `base_purview`, `menu_purview`, `status`) VALUES
(1, '网站管理员', 'a:2:{i:0;s:15:"Admin_AppManage";i:1;s:21:"Admin_AppManage_index";}', 'a:4:{i:0;s:19:"首页_管理首页";i:1;s:19:"内容_栏目管理";i:2;s:19:"内容_文章管理";i:3;s:22:"系统_用户组管理";}', 1),
(2, '科室工作人员', 'N;', 'a:1:{i:0;s:19:"内容_文章管理";}', 1),
(3, '科室管理员', 'N;', 'a:5:{i:0;s:19:"首页_站点统计";i:1;s:19:"首页_安全中心";i:2;s:19:"内容_栏目管理";i:3;s:19:"内容_文章管理";i:4;s:22:"功能_推荐位管理";}', 1);

-- --------------------------------------------------------

--
-- 表的结构 `dux_admin_log`
--

CREATE TABLE IF NOT EXISTS `dux_admin_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `ip` varchar(250) DEFAULT NULL,
  `app` varchar(250) DEFAULT '1',
  `content` text,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='后台操作记录' AUTO_INCREMENT=36 ;

--
-- 转存表中的数据 `dux_admin_log`
--

INSERT INTO `dux_admin_log` (`log_id`, `user_id`, `time`, `ip`, `app`, `content`) VALUES
(1, 1, 1418103836, '0.0.0.0', 'Admin', '登录系统'),
(2, 1, 1418191384, '0.0.0.0', 'Admin', '登录系统'),
(3, 1, 1418200796, '0.0.0.0', 'Admin', '登录系统'),
(4, 1, 1418200817, '0.0.0.0', 'Admin', '登录系统'),
(5, 1, 1418200897, '0.0.0.0', 'Admin', '登录系统'),
(6, 1, 1418200957, '0.0.0.0', 'Admin', '登录系统'),
(7, 1, 1418201358, '0.0.0.0', 'Admin', '登录系统'),
(8, 1, 1418201381, '0.0.0.0', 'Admin', '登录系统'),
(9, 1, 1418204674, '0.0.0.0', 'Admin', '登录系统'),
(10, 1, 1418205077, '0.0.0.0', 'Admin', '登录系统'),
(11, 1, 1418205252, '0.0.0.0', 'Admin', '登录系统'),
(12, 1, 1418205814, '0.0.0.0', 'Admin', '登录系统'),
(13, 1, 1418205846, '0.0.0.0', 'Admin', '登录系统'),
(14, 1, 1418206059, '0.0.0.0', 'Admin', '登录系统'),
(15, 1, 1418206094, '0.0.0.0', 'Admin', '登录系统'),
(16, 1, 1418206194, '0.0.0.0', 'Admin', '登录系统'),
(17, 1, 1418206250, '0.0.0.0', 'Admin', '登录系统'),
(18, 1, 1418206354, '0.0.0.0', 'Admin', '登录系统'),
(19, 1, 1418206461, '0.0.0.0', 'Admin', '登录系统'),
(20, 1, 1418206511, '0.0.0.0', 'Admin', '登录系统'),
(21, 1, 1418206562, '0.0.0.0', 'Admin', '登录系统'),
(22, 1, 1418207278, '0.0.0.0', 'Admin', '登录系统'),
(23, 1, 1418207367, '0.0.0.0', 'Admin', '登录系统'),
(24, 1, 1418207432, '0.0.0.0', 'Admin', '登录系统'),
(25, 1, 1418207452, '0.0.0.0', 'Admin', '登录系统'),
(26, 1, 1418207490, '0.0.0.0', 'Admin', '登录系统'),
(27, 1, 1418298192, '0.0.0.0', 'Admin', '登录系统'),
(28, 1, 1418298749, '0.0.0.0', 'Admin', '登录系统'),
(29, 1, 1418299021, '0.0.0.0', 'Admin', '登录系统'),
(30, 1, 1418299151, '0.0.0.0', 'Admin', '登录系统'),
(31, 1, 1418299320, '0.0.0.0', 'Admin', '登录系统'),
(32, 1, 1418299454, '0.0.0.0', 'Admin', '登录系统'),
(33, 1, 1418299606, '0.0.0.0', 'Admin', '登录系统'),
(34, 1, 1418301154, '0.0.0.0', 'Admin', '登录系统'),
(35, 1, 1418367623, '0.0.0.0', 'Admin', '登录系统');

-- --------------------------------------------------------

--
-- 表的结构 `dux_admin_user`
--

CREATE TABLE IF NOT EXISTS `dux_admin_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户IP',
  `group_id` int(10) NOT NULL DEFAULT '1' COMMENT '用户组ID',
  `username` varchar(20) NOT NULL COMMENT '登录名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `nicename` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态',
  `level` int(5) DEFAULT '1' COMMENT '等级',
  `reg_time` int(10) DEFAULT NULL COMMENT '注册时间',
  `last_login_time` int(10) DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(15) DEFAULT '未知' COMMENT '登录IP',
  PRIMARY KEY (`user_id`),
  KEY `username` (`username`),
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='后台管理员' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `dux_admin_user`
--

INSERT INTO `dux_admin_user` (`user_id`, `group_id`, `username`, `password`, `nicename`, `email`, `status`, `level`, `reg_time`, `last_login_time`, `last_login_ip`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', 'admin@duxcms.com', 1, 1, 1399361747, 1418367623, '0.0.0.0'),
(2, 2, 'why2012', '81dc9bdb52d04dc20036dbd8313ed055', 'nick', '12@12.a.com', 1, 1, 1418201349, 1418201358, '0.0.0.0');

-- --------------------------------------------------------

--
-- 表的结构 `dux_category`
--

CREATE TABLE IF NOT EXISTS `dux_category` (
  `class_id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT '0',
  `app` varchar(20) DEFAULT NULL,
  `show` tinyint(1) unsigned DEFAULT '1',
  `sequence` int(10) DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  `urlname` varchar(250) DEFAULT NULL,
  `subname` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `class_tpl` varchar(250) DEFAULT NULL,
  `keywords` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `urlname` (`urlname`) USING BTREE,
  KEY `pid` (`parent_id`),
  KEY `mid` (`app`),
  KEY `sequence` (`sequence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='栏目基础信息' AUTO_INCREMENT=25 ;

--
-- 转存表中的数据 `dux_category`
--

INSERT INTO `dux_category` (`class_id`, `parent_id`, `app`, `show`, `sequence`, `name`, `urlname`, `subname`, `image`, `class_tpl`, `keywords`, `description`) VALUES
(1, 0, 'Article', 1, 1, '机构架设', 'jigoujiashe', '', '', 'list', '', ''),
(2, 0, 'Article', 1, 1, '政策法规', 'zhengcefagui', '', '', 'list', '', ''),
(3, 0, 'Article', 1, 1, '党建工作', 'dangjiangongzuo', '', '', 'list', '', ''),
(4, 0, 'Article', 1, 1, '关工委', 'guangongwei', '', '', 'list', '', ''),
(5, 0, 'Article', 1, 1, '老年社团', 'laonianshetuan', '', '', 'list', '', ''),
(6, 0, 'Article', 1, 1, '生活百科', 'shenghuobaike', '', '', 'list', '', ''),
(7, 0, 'Article', 1, 1, '服务指南', 'fuwuzhinan', '', '', 'list', '', ''),
(8, 0, 'Article', 1, 1, '资料下载', 'ziliaoxiazai', '', '', 'list', '', ''),
(9, 0, 'Article', 1, 1, '最新通知', 'zuixintongzhi', '', '', 'list', '', ''),
(10, 0, 'Article', 1, 1, '图片新闻', 'tupianxinwen', '', '', 'list', '', ''),
(11, 0, 'Article', 1, 1, '工作动态', 'gongzuodongtai', '', '', 'list', '', ''),
(12, 0, 'Article', 1, 1, '成电记忆', 'chengdianjiyi', '', '', 'list', '', ''),
(13, 1, 'Page', 1, 1, '部门简介', 'bumenjianjie', '', '', 'page', '', ''),
(14, 1, 'Page', 1, 1, '部门领导', 'bumenlingdao', '', '', 'list', '', ''),
(15, 1, 'Page', 1, 1, '内设机构', 'neishejigou', '', '', 'list', '', ''),
(16, 3, 'Article', 1, 1, '组织建设', 'zuzhijianshe', '', '', 'list', '', ''),
(17, 3, 'Article', 1, 1, '党建活动', 'dangjianhuodong', '', '', 'list', '', ''),
(18, 3, 'Article', 1, 1, '支部生活', 'zhibushenghuo', '', '', 'list', '', ''),
(19, 4, 'Page', 1, 1, '关工委简介', 'guangongweijianjie', '', '', 'list', '', ''),
(20, 4, 'Page', 1, 1, '组织机构', 'zuzhijigou', '', '', 'list', '', ''),
(21, 4, 'Page', 1, 1, '活动介绍', 'huodongjieshao', '', '', 'list', '', ''),
(22, 5, 'Page', 1, 1, '社团简介', 'shetuanjianjie', '', '', 'list', '', ''),
(23, 5, 'Article', 1, 1, '社团动态', 'shetuandongtai', '', '', 'list', '', ''),
(24, 5, 'Article', 1, 1, '活动集锦', 'huodongjijin', '', '', 'list', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `dux_category_article`
--

CREATE TABLE IF NOT EXISTS `dux_category_article` (
  `class_id` int(10) NOT NULL,
  `fieldset_id` int(10) NOT NULL,
  `type` int(10) NOT NULL DEFAULT '1',
  `content_tpl` varchar(250) NOT NULL,
  `config_upload` text NOT NULL,
  `content_order` varchar(250) NOT NULL,
  `page` int(10) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章栏目信息';

--
-- 转存表中的数据 `dux_category_article`
--

INSERT INTO `dux_category_article` (`class_id`, `fieldset_id`, `type`, `content_tpl`, `config_upload`, `content_order`, `page`) VALUES
(1, 0, 1, 'content', '', 'time DESC', 20),
(2, 0, 1, 'content', '', 'time DESC', 20),
(3, 0, 1, 'content', '', 'time DESC', 20),
(4, 0, 1, 'content', '', 'time DESC', 20),
(5, 0, 1, 'content', '', 'time DESC', 20),
(6, 0, 1, 'content', '', 'time DESC', 20),
(7, 0, 1, 'content', '', 'time DESC', 20),
(8, 0, 1, 'content', '', 'time DESC', 20),
(9, 0, 1, 'content', '', 'time DESC', 20),
(10, 0, 1, 'content', '', 'time DESC', 20),
(11, 0, 1, 'content', '', 'time DESC', 20),
(12, 0, 1, 'content', '', 'time DESC', 20),
(16, 0, 1, 'content', '', 'time DESC', 20),
(17, 0, 1, 'content', '', 'time DESC', 20),
(18, 0, 1, 'content', '', 'time DESC', 20),
(23, 0, 1, 'content', '', 'time DESC', 20),
(24, 0, 1, 'content', '', 'time DESC', 20);

-- --------------------------------------------------------

--
-- 表的结构 `dux_category_page`
--

CREATE TABLE IF NOT EXISTS `dux_category_page` (
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext COMMENT '内容',
  KEY `cid` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单页栏目信息';

--
-- 转存表中的数据 `dux_category_page`
--

INSERT INTO `dux_category_page` (`class_id`, `content`) VALUES
(13, '部门简介'),
(14, '部门领导'),
(15, '内设机构'),
(19, '关工委简介'),
(20, '组织机构'),
(21, '活动介绍'),
(22, '社团简介');

-- --------------------------------------------------------

--
-- 表的结构 `dux_config`
--

CREATE TABLE IF NOT EXISTS `dux_config` (
  `name` varchar(250) NOT NULL,
  `data` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站配置';

--
-- 转存表中的数据 `dux_config`
--

INSERT INTO `dux_config` (`name`, `data`) VALUES
('site_title', '离退休工作处'),
('site_subtitle', '电子科技大学'),
('site_url', ''),
('site_keywords', '电子科技大学,离退休工作处,uestc,'),
('site_description', '电子科技大学离退休工作处'),
('site_email', 'admin@duxcms.com'),
('site_copyright', 'why'),
('site_statistics', ''),
('tpl_name', 'uestcra'),
('tpl_index', 'index'),
('tpl_search', 'search'),
('tpl_tags', 'tag'),
('upload_size', '10'),
('upload_exts', 'jpg,gif,png,bmp'),
('upload_replace', '1'),
('thumb_status', '0'),
('water_status', '0'),
('thumb_type', '3'),
('thumb_width', '500'),
('thumb_height', '500'),
('water_image', 'logo.png'),
('water_position', '2'),
('mobile_status', '0'),
('mobile_tpl', 'mobile'),
('mobile_domain', '');

-- --------------------------------------------------------

--
-- 表的结构 `dux_content`
--

CREATE TABLE IF NOT EXISTS `dux_content` (
  `content_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `class_id` int(10) DEFAULT NULL COMMENT '栏目ID',
  `title` varchar(250) DEFAULT NULL COMMENT '标题',
  `urltitle` varchar(250) DEFAULT NULL COMMENT 'URL路径',
  `font_color` varchar(250) DEFAULT NULL COMMENT '颜色',
  `font_bold` tinyint(1) DEFAULT NULL COMMENT '加粗',
  `font_em` tinyint(1) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `keywords` varchar(250) DEFAULT NULL COMMENT '关键词',
  `description` varchar(250) DEFAULT NULL COMMENT '描述',
  `time` int(10) DEFAULT NULL COMMENT '更新时间',
  `image` varchar(250) DEFAULT NULL COMMENT '封面图',
  `url` varchar(250) DEFAULT NULL COMMENT '跳转',
  `sequence` int(10) DEFAULT NULL COMMENT '排序',
  `status` int(10) DEFAULT NULL COMMENT '状态',
  `copyfrom` varchar(250) DEFAULT NULL COMMENT '来源',
  `views` int(10) DEFAULT '0' COMMENT '浏览数',
  `taglink` int(10) DEFAULT '0' COMMENT 'TAG链接',
  `tpl` varchar(250) DEFAULT NULL,
  `site` int(10) DEFAULT '1',
  PRIMARY KEY (`content_id`),
  UNIQUE KEY `urltitle` (`urltitle`) USING BTREE,
  KEY `title` (`title`) USING BTREE,
  KEY `description` (`description`) USING BTREE,
  KEY `keywords` (`keywords`),
  KEY `class_id` (`class_id`) USING BTREE,
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='内容基础' AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `dux_content`
--

INSERT INTO `dux_content` (`content_id`, `class_id`, `title`, `urltitle`, `font_color`, `font_bold`, `font_em`, `position`, `keywords`, `description`, `time`, `image`, `url`, `sequence`, `status`, `copyfrom`, `views`, `taglink`, `tpl`, `site`) VALUES
(1, 9, '测试', 'ceshi', NULL, NULL, NULL, NULL, '', '', 1418196300, '', NULL, 0, 1, '本站', 11, 0, '', 1),
(2, 10, '测试', 'ceshi50521001', NULL, NULL, NULL, NULL, '', '', 1418200920, '', NULL, 0, 1, '本站', 15, 0, '', 1),
(3, 11, '测试', 'ceshi53484850', NULL, NULL, NULL, NULL, '', '', 1418200920, '', NULL, 0, 1, '本站', 1, 0, '', 1),
(4, 3, '测试', 'ceshi48575450', NULL, NULL, NULL, NULL, '', '', 1418200980, '', NULL, 0, 1, '本站', 12, 0, '', 1),
(5, 4, '测试', 'ceshi51101102', NULL, NULL, NULL, NULL, '', '', 1418200980, '', NULL, 0, 1, '本站', 1, 0, '', 1),
(6, 5, '测试', 'ceshi10257100', NULL, NULL, NULL, NULL, '', '', 1418200980, '', NULL, 0, 1, '本站', 1, 0, '', 1),
(7, 12, 'Pic1', 'Pic1', NULL, NULL, NULL, NULL, '', '', 1418298180, '/uestcra/Uploads/2014-12-11/548983c89c53d.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(8, 12, 'Pic2', 'Pic2', NULL, NULL, NULL, NULL, '', '', 1418298300, '/uestcra/Uploads/2014-12-11/5489840c564f0.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(9, 12, 'Pic3', 'Pic3', NULL, NULL, NULL, NULL, '', '', 1418298360, '/uestcra/Uploads/2014-12-11/548984178a7cc.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(10, 12, 'Pic4', 'Pic4', NULL, NULL, NULL, NULL, '', '', 1418298360, '/uestcra/Uploads/2014-12-11/5489842161d19.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(11, 12, 'Pic5', 'Pic5', NULL, NULL, NULL, NULL, '', '', 1418298360, '/uestcra/Uploads/2014-12-11/5489842e011b9.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(12, 12, 'Pic6', 'Pic6', NULL, NULL, NULL, NULL, '', '', 1418298360, '/uestcra/Uploads/2014-12-11/5489843725aea.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(13, 12, 'Pic7', 'Pic7', NULL, NULL, NULL, NULL, '', '', 1418298420, '/uestcra/Uploads/2014-12-11/5489843f6fffc.jpg', NULL, 0, 1, '本站', 0, 0, '', 1),
(14, 3, '测试', 'ceshi53100509', NULL, NULL, NULL, NULL, '', '', 1418367600, '', NULL, 0, 1, '本站', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `dux_content_article`
--

CREATE TABLE IF NOT EXISTS `dux_content_article` (
  `content_id` int(10) DEFAULT NULL,
  `content` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章内容信息';

--
-- 转存表中的数据 `dux_content_article`
--

INSERT INTO `dux_content_article` (`content_id`, `content`) VALUES
(1, '测试'),
(2, '测试&lt;img src=&quot;/uestcra/Uploads/2014-12-10/54881cc8c99b8.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-10/5488197c1d087.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-10/54881551e273f.jpg&quot; alt=&quot;&quot; /&gt;测试'),
(3, '测试'),
(4, '&lt;img src=&quot;/uestcra/Uploads/2014-12-11/548992fe62014.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-11/5489921a43134.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-11/548989c048ea1.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-11/54898ef51a744.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-11/54898986e268f.jpg&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/uestcra/Uploads/2014-12-11/548988ed78fb0.jpg&quot; alt=&quot;&quot; /&gt;测试 1'),
(5, '测试'),
(6, '测试'),
(7, ''),
(8, ''),
(9, ''),
(10, ''),
(11, ''),
(12, ''),
(13, ''),
(14, '测试');

-- --------------------------------------------------------

--
-- 表的结构 `dux_field`
--

CREATE TABLE IF NOT EXISTS `dux_field` (
  `field_id` int(10) NOT NULL AUTO_INCREMENT,
  `fieldset_id` int(10) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `field` varchar(250) DEFAULT NULL,
  `type` int(5) DEFAULT '1',
  `tip` varchar(250) DEFAULT NULL,
  `verify_type` varchar(250) DEFAULT NULL,
  `verify_data` text,
  `verify_data_js` text,
  `verify_condition` tinyint(1) DEFAULT NULL,
  `default` varchar(250) DEFAULT NULL,
  `sequence` int(10) DEFAULT '0',
  `errormsg` varchar(250) DEFAULT NULL,
  `config` text,
  PRIMARY KEY (`field_id`),
  KEY `field` (`field`),
  KEY `sequence` (`sequence`),
  KEY `fieldset_id` (`fieldset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展字段基础' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_field_expand`
--

CREATE TABLE IF NOT EXISTS `dux_field_expand` (
  `field_id` int(10) DEFAULT NULL,
  `post` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展字段-扩展模型';

-- --------------------------------------------------------

--
-- 表的结构 `dux_field_form`
--

CREATE TABLE IF NOT EXISTS `dux_field_form` (
  `field_id` int(10) DEFAULT NULL,
  `post` tinyint(1) DEFAULT '0',
  `show` tinyint(1) DEFAULT '0',
  `search` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扩展字段-表单';

-- --------------------------------------------------------

--
-- 表的结构 `dux_fieldset`
--

CREATE TABLE IF NOT EXISTS `dux_fieldset` (
  `fieldset_id` int(10) NOT NULL AUTO_INCREMENT,
  `table` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`fieldset_id`),
  UNIQUE KEY `table` (`table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段集基础' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_fieldset_expand`
--

CREATE TABLE IF NOT EXISTS `dux_fieldset_expand` (
  `fieldset_id` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  UNIQUE KEY `fieldset_id` (`fieldset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段集-扩展模型';

-- --------------------------------------------------------

--
-- 表的结构 `dux_fieldset_form`
--

CREATE TABLE IF NOT EXISTS `dux_fieldset_form` (
  `fieldset_id` int(10) DEFAULT NULL,
  `show_list` tinyint(1) DEFAULT NULL,
  `show_info` tinyint(1) DEFAULT NULL,
  `list_page` int(5) DEFAULT NULL,
  `list_where` varchar(250) DEFAULT NULL,
  `list_order` varchar(250) DEFAULT NULL,
  `tpl_list` varchar(250) DEFAULT NULL,
  `tpl_info` varchar(250) DEFAULT NULL,
  `post_status` tinyint(1) DEFAULT NULL,
  `post_msg` varchar(250) DEFAULT NULL,
  `post_return_url` varchar(250) DEFAULT NULL,
  UNIQUE KEY `fieldset_id` (`fieldset_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段集-扩展表单';

-- --------------------------------------------------------

--
-- 表的结构 `dux_file`
--

CREATE TABLE IF NOT EXISTS `dux_file` (
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) DEFAULT NULL,
  `original` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `ext` varchar(250) DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `ext` (`ext`),
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='上传文件' AUTO_INCREMENT=34 ;

--
-- 转存表中的数据 `dux_file`
--

INSERT INTO `dux_file` (`file_id`, `url`, `original`, `title`, `ext`, `size`, `time`) VALUES
(1, '/uestcra/Uploads/2014-12-10/54881551e273f.jpg', '/uestcra/Uploads/2014-12-10/54881551e273f.jpg', '1.jpg', 'jpg', 317056, 1418204497),
(2, '/uestcra/Uploads/2014-12-10/548816a4bf281.jpg', '/uestcra/Uploads/2014-12-10/548816a4bf281.jpg', '1.jpg', 'jpg', 317056, 1418204836),
(3, '/uestcra/Uploads/2014-12-10/548816aea3c0a.jpg', '/uestcra/Uploads/2014-12-10/548816aea3c0a.jpg', '2.jpg', 'jpg', 193308, 1418204846),
(4, '/uestcra/Uploads/2014-12-10/5488178968428.jpg', '/uestcra/Uploads/2014-12-10/5488178968428.jpg', '2.jpg', 'jpg', 193308, 1418205065),
(5, '/uestcra/Uploads/2014-12-10/548817c30c17d.jpg', '/uestcra/Uploads/2014-12-10/548817c30c17d.jpg', '3.jpg', 'jpg', 113654, 1418205123),
(6, '/uestcra/Uploads/2014-12-10/5488197c1d087.jpg', '/uestcra/Uploads/2014-12-10/5488197c1d087.jpg', '1.jpg', 'jpg', 317056, 1418205564),
(7, '/uestcra/Uploads/2014-12-10/54881cc8c99b8.jpg', '/uestcra/Uploads/2014-12-10/54881cc8c99b8.jpg', '2.jpg', 'jpg', 193308, 1418206408),
(8, '/uestcra/Uploads/2014-12-10/54882010f0a22.jpg', '/uestcra/Uploads/2014-12-10/54882010f0a22.jpg', '3.jpg', 'jpg', 113654, 1418207248),
(9, '/uestcra/Uploads/2014-12-10/5488207975f05.jpg', '/uestcra/Uploads/2014-12-10/5488207975f05.jpg', '3.jpg', 'jpg', 113654, 1418207353),
(10, '/uestcra/Uploads/2014-12-10/548820c1242f1.jpg', '/uestcra/Uploads/2014-12-10/548820c1242f1.jpg', '3.jpg', 'jpg', 113654, 1418207425),
(11, '/uestcra/Uploads/2014-12-10/548820d45204f.jpg', '/uestcra/Uploads/2014-12-10/548820d45204f.jpg', '3.jpg', 'jpg', 113654, 1418207444),
(12, '/uestcra/Uploads/2014-12-10/548820fd19e1c.jpg', '/uestcra/Uploads/2014-12-10/548820fd19e1c.jpg', '3.jpg', 'jpg', 113654, 1418207485),
(13, '/uestcra/Uploads/2014-12-11/548983c89c53d.jpg', '/uestcra/Uploads/2014-12-11/548983c89c53d.jpg', '1.jpg', 'jpg', 317056, 1418298312),
(14, '/uestcra/Uploads/2014-12-11/5489840c564f0.jpg', '/uestcra/Uploads/2014-12-11/5489840c564f0.jpg', '1.jpg', 'jpg', 24321, 1418298380),
(15, '/uestcra/Uploads/2014-12-11/548984178a7cc.jpg', '/uestcra/Uploads/2014-12-11/548984178a7cc.jpg', 'a.jpg', 'jpg', 25691, 1418298391),
(16, '/uestcra/Uploads/2014-12-11/5489842161d19.jpg', '/uestcra/Uploads/2014-12-11/5489842161d19.jpg', 'news-image-1.jpg', 'jpg', 10696, 1418298401),
(17, '/uestcra/Uploads/2014-12-11/5489842e011b9.jpg', '/uestcra/Uploads/2014-12-11/5489842e011b9.jpg', 'slide-1.jpg', 'jpg', 98787, 1418298414),
(18, '/uestcra/Uploads/2014-12-11/5489843725aea.jpg', '/uestcra/Uploads/2014-12-11/5489843725aea.jpg', 'u.jpg', 'jpg', 63089, 1418298423),
(19, '/uestcra/Uploads/2014-12-11/5489843f6fffc.jpg', '/uestcra/Uploads/2014-12-11/5489843f6fffc.jpg', 'x.jpg', 'jpg', 38609, 1418298431),
(20, '/uestcra/Uploads/2014-12-11/5489856b9c23a.jpg', '/uestcra/Uploads/2014-12-11/5489856b9c23a.jpg', '1.jpg', 'jpg', 24321, 1418298731),
(21, '/uestcra/Uploads/2014-12-11/5489867b199d2.jpg', '/uestcra/Uploads/2014-12-11/5489867b199d2.jpg', '1.jpg', 'jpg', 24321, 1418299003),
(22, '/uestcra/Uploads/2014-12-11/5489870728f3b.jpg', '/uestcra/Uploads/2014-12-11/5489870728f3b.jpg', '1.jpg', 'jpg', 24321, 1418299143),
(23, '/uestcra/Uploads/2014-12-11/5489878f18ee5.jpg', '/uestcra/Uploads/2014-12-11/5489878f18ee5.jpg', '1.jpg', 'jpg', 24321, 1418299279),
(24, '/uestcra/Uploads/2014-12-11/5489882e92fa0.jpg', '/uestcra/Uploads/2014-12-11/5489882e92fa0.jpg', '1.jpg', 'jpg', 24321, 1418299438),
(25, '/uestcra/Uploads/2014-12-11/5489882eab359.jpg', '/uestcra/Uploads/2014-12-11/5489882eab359.jpg', 'a.jpg', 'jpg', 25691, 1418299438),
(26, '/uestcra/Uploads/2014-12-11/548988ed78fb0.jpg', '/uestcra/Uploads/2014-12-11/548988ed78fb0.jpg', '1.jpg', 'jpg', 24321, 1418299629),
(27, '/uestcra/Uploads/2014-12-11/5489891a93874.jpg', '/uestcra/Uploads/2014-12-11/5489891a93874.jpg', '1.jpg', 'jpg', 24321, 1418299674),
(28, '/uestcra/Uploads/2014-12-11/54898937e3b00.jpg', '/uestcra/Uploads/2014-12-11/54898937e3b00.jpg', 'a.jpg', 'jpg', 25691, 1418299703),
(29, '/uestcra/Uploads/2014-12-11/54898986e268f.jpg', '/uestcra/Uploads/2014-12-11/54898986e268f.jpg', 'a.jpg', 'jpg', 25691, 1418299782),
(30, '/uestcra/Uploads/2014-12-11/548989c048ea1.jpg', '/uestcra/Uploads/2014-12-11/548989c048ea1.jpg', 'x.jpg', 'jpg', 38609, 1418299840),
(31, '/uestcra/Uploads/2014-12-11/54898ef51a744.jpg', '/uestcra/Uploads/2014-12-11/54898ef51a744.jpg', 'u.jpg', 'jpg', 63089, 1418301173),
(32, '/uestcra/Uploads/2014-12-11/5489921a43134.jpg', '/uestcra/Uploads/2014-12-11/5489921a43134.jpg', 'slide-1.jpg', 'jpg', 98787, 1418301978),
(33, '/uestcra/Uploads/2014-12-11/548992fe62014.jpg', '/uestcra/Uploads/2014-12-11/548992fe62014.jpg', 'a.jpg', 'jpg', 25691, 1418302206);

-- --------------------------------------------------------

--
-- 表的结构 `dux_fragment`
--

CREATE TABLE IF NOT EXISTS `dux_fragment` (
  `fragment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `label` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`fragment_id`),
  KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站碎片' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_position`
--

CREATE TABLE IF NOT EXISTS `dux_position` (
  `position_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `sequence` int(10) DEFAULT '0',
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='推荐位' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `dux_position`
--

INSERT INTO `dux_position` (`position_id`, `name`, `sequence`) VALUES
(1, '首页推荐', 0);

-- --------------------------------------------------------

--
-- 表的结构 `dux_tags`
--

CREATE TABLE IF NOT EXISTS `dux_tags` (
  `tag_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `click` int(10) DEFAULT '1',
  `quote` int(10) DEFAULT '1',
  PRIMARY KEY (`tag_id`),
  KEY `quote` (`quote`),
  KEY `click` (`click`),
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='TAG标签' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_tags_has`
--

CREATE TABLE IF NOT EXISTS `dux_tags_has` (
  `content_id` int(10) DEFAULT NULL,
  `tag_id` int(10) DEFAULT NULL,
  KEY `aid` (`content_id`),
  KEY `tid` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='TAG关系';

-- --------------------------------------------------------

--
-- 表的结构 `dux_total_spider`
--

CREATE TABLE IF NOT EXISTS `dux_total_spider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` int(10) DEFAULT NULL,
  `baidu` int(10) DEFAULT '0',
  `google` int(10) DEFAULT '0',
  `soso` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='蜘蛛统计' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_total_visitor`
--

CREATE TABLE IF NOT EXISTS `dux_total_visitor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` int(10) DEFAULT NULL,
  `count` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='访客统计' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `dux_total_visitor`
--

INSERT INTO `dux_total_visitor` (`id`, `time`, `count`) VALUES
(1, 1418054400, 286),
(2, 1418140800, 420),
(3, 1418227200, 69),
(4, 1418313600, 34);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
